﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public string apiUrl = "http://localhost:3000";
        private void Load_dropdown()
        { 
            List<taxcode> objOrder = (new JavaScriptSerializer()).Deserialize<List<taxcode>>(RequestRespons(apiUrl + "/taxcode"));
            ddl_type.DataSource = objOrder;
            ddl_type.DataTextField = "name";
            ddl_type.DataValueField = "id";
            ddl_type.DataBind();
        }
        private void Load_Table()
        { 
        
           
            List<Order>  objOrder =  (new JavaScriptSerializer()).Deserialize<List<Order>>(RequestRespons(apiUrl + "/orderitem/getall"));
            tbl_grdView.DataSource = objOrder;
            tbl_grdView.DataBind();
        }

        private string RequestRespons(string url)
        {
            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
            httpRequest.ContentType = "application/json";
            httpRequest.Method = "GET";

            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            Stream stream = httpResponse.GetResponseStream();

            return (new StreamReader(stream)).ReadToEnd();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            Load_dropdown();
            Load_Table();

        }

        protected void btn_submi_Click(object sender, EventArgs e)
        {
            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(new Uri(apiUrl + "/orderitem/order"));
            httpRequest.ContentType = "application/json";
            httpRequest.Method = "POST";
            Order objOrder = new Order();
            objOrder.itemName = txt_itemName.Text;
            objOrder.Price = Convert.ToInt32( txt_price.Text);
            objOrder.Tax_Code = Convert.ToInt32(ddl_type.SelectedValue);
            string jsonstring  =(new JavaScriptSerializer()).Serialize(objOrder);
            using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonstring);
                streamWriter.Flush();
                streamWriter.Close();
            }

            
            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            Stream stream = httpResponse.GetResponseStream();
            

        }
    }
    public class Order {
        public string itemName { set; get; }
        public int Tax_Code { set; get; }
        public string taxName { set; get; }
        public bool Refundable{set;get;}
        public int Price{set;get;} 
        public int Tax{set;get;}
        public int Amount {set;get;}
    }
    public class taxcode
    {

        public int id { set; get; }
        public string name { set; get; }
    }
}