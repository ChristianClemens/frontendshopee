﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Item Name
        <asp:textbox id="txt_itemName" runat="server" />
        Price
        <asp:textbox id="txt_price" runat="server" />
        Type 
        <asp:DropDownList ID="ddl_type" runat="server" />
        <asp:Button Text="submit" ID="btn_submi" runat="server" OnClick="btn_submi_Click"/>

        
    </div>
        <div>
        <asp:GridView ID="tbl_grdView" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="itemName" HeaderText="Name" />
                <asp:BoundField DataField="Tax_Code" HeaderText="Tax_Code" />
                <asp:BoundField DataField="taxName" HeaderText="Type" />
                <asp:BoundField DataField="Refundable" HeaderText="Refundable" />
                <asp:BoundField DataField="Price" HeaderText="Price" />
                <asp:BoundField DataField="Tax" HeaderText="Tax" />
                <asp:BoundField DataField="Amount" HeaderText="Amount" />
            </Columns>
        </asp:GridView>
    </div>
    </form>
    
</body>
</html>
